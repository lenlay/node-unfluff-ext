module.exports = embeds = (doc, topNode) ->
  extractInstagram(doc, topNode)
  extractTwitter(doc, topNode)
  extractBiYoutube(doc, topNode)

  return topNode

extractBiYoutube = (doc, topNode) ->
  nodes = doc(topNode).find('biembeddedobject.youtube')
  regex = /(?:http(?:s?):)?\/\/(?:www\.)?youtu(?:be\.com\/watch\?v=|\.be\/|be\.com\/embed\/(?:watch)?)([\w\-\_]*)([\w\=;\?&]*)?/gmi

  nodes.each () ->
    html = doc(this).html()
    match = regex.exec(html)
    if match
      link = match[0]
    else
      link = false

    if !link
      doc(this).html('')
    else
      doc(this).replaceWith(instagramEmbedWp(link))

extractInstagram = (doc, topNode) ->
  nodes = doc(topNode).find('blockquote.instagram-media')

  nodes.each () ->
    link = doc(this).data('instgrm-permalink')
    doc(this).replaceWith(instagramEmbedWp(link))

extractTwitter = (doc, topNode) ->
  nodes = doc(topNode).find('blockquote.twitter-tweet')
    
  nodes.each () ->
    found = false
    links = doc(this).find('a')
    links.each () ->
      if doc(this).attr('href').indexOf('/status/') != -1
        found = doc(this)
    if found
      doc(this).replaceWith(twitterEmbedWp(found.attr('href')))

instagramEmbedWp = (link) ->
  template = "[embed]#{link}[/embed]"

  template

twitterEmbedWp = (link) ->
  template = "[embed]#{link}[/embed]"

  template
